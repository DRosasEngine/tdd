# Network Layer Testing

Here you'll see:
- Tools for testing the network layer.
- How to provide reliable test data.
- Important things to know about mantaining network layer tests.

When you run the automated tests, you don't actually want tomake a network call. You need your tests to be repeatable and predictable.
The useful tools we'are gonna see are **MockWebServer**, **Mockito** and **Faker**.

To keep your tests repeatable and predictable, you shouldn't make real network calls in your tests.

Deciding which tools are right for the job takes time to learn through experiment, trial and error.


## MockWebServer

Is a library from OkHttp that allows you to run a local HTTP server in your tests. You can specify what you want the server to return and perform verifications on the request made. Very useful to script request responses and verify that the correct endpoint was called.

Could be used like here:

``` 
    private val retrofit by lazy {
    Retrofit.Builder()
        .baseUrl(mockWebServer.url("/"))
        .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
        .addConverterFactory(GsonConverterFactory.create())
        .build()
    }
```

## Provide test data 

It's possible to script a result. There are two ways to set up the JSON to script a response with. One way is to pull in the JSON from a file. This is a great option if you have a long expected response or you have a real response from a request that you want to copy paste in. The other way is to create a JSON String in the test file. Like this:

```private val testJson = """{ "id": 1, "joke": "joke" }"""```

and add to the mockWebServer like this:

```
    mockWebServer.enqueue(
        MockResponse()
            .setBody(testJson)
            .setResponseCode(200))
````

## Mock the service

This tests the interactions with the network layer instead of the network calls themselves.

```
@Test
    fun getRandomJokeEmitsJoke(){
        val joke = Joke(id, joke)
        whenever(jokeService.getRandomJoke()).thenReturn(Single.just(joke))
        val testObserver = repository.getJoke().test()
        testObserver.assertValue(joke)
    }
```

## Faker for test data
Faker is a library to generate random and interesting test data. Many of theFaker methods return an object that0s part of the library. It's from the objects that you can get a value.

