# TDD
## Introduction
Test-Driven Development, also known as TDD, is one way of ensuring to include tests with any new code. When following this process, you write the tests for the thing you are adding before you write the code to implement it. Using TDD when developing an Android app is what you will learn in this tutorial, and by the end, you’ll understand:
- The features and uses of TDD.
- Why TDD is useful.
- The steps for practicing TDD.
- How to use TDD in your own projects.
- How to write tests for ViewModel instances that use LiveData, both part of the Architecture Components from Google.

### What is TDD?
TDD is a software-development process in which you write the tests that specify the code you’re going to write before you start writing any of the code.

### Why is TDD important?
- **Faster development time**: When you have well-written tests, they provide an excellent description of what your code should do. From the start, you have the end goal in mind.
- **Automatic, up-to-date documentation**: When you’re coming into a piece of code, you can look at the tests to help you understand what the code does.
- **More maintainable code**: When practicing TDD, it encourages you to pay attention to the structure of your code.
- **Greater confidence in your code**: Tests help you to ensure that your code works the way it should. Because of this, you can have greater confidence that what you wrote is “complete.”
- **Higher test coverage**

### TDD in 5 steps
You usually want a test for the happy path and at least one sad path. If there is a method with a lot of branching, it’s ideal to have a test for each of the branches.

You accomplish TDD by following five steps:

1. **Add a test**: Anytime you start a new feature, fix or refactor, you write a test for it. This test will specify how this change or addition should behave. You only write the test at this step and just enough code to make it compile.
2. **Run it and watch it fail**: Here, you run the tests. If you did step one correctly, these tests should fail.
3. **Write the code to make the test pass**: This is when you write your feature, fix or refactor. It will follow the specifications you laid down in the tests.
4. **Run the tests and see them pass**: Now, you get to run the tests again! At this point, they should pass. If they don’t, go back to step three until all your tests are green!
5. **Do any refactoring**: Now that you have a test that makes sure your implementation matches the specifications, you can adjust and refactor the implementation that you have to ensure that it’s clean and structured the way you want without any worries that you’ll break what you just wrote.

### Test types
#### Instrumental
Parts of the code that are dependent on the Android framework but that do not require the UI. These need an emulator or physical device to run because of this dependency.
These tests go in a app/src/androidTest/ directory with the same package structure as your project.

#### Unit
Focuses on the small building blocks of your code. It’s generally concerned with one class at a time, testing one function at a time. Small and independent of the Android framework and so do not need to run on a device or emulator. JUnit is usually used to run these tests.
Because unit tests are independent of the Android framework, they generally go in the app/src/test/ directory with the same package structure as your project.

#### UI
Test what the user sees on the screen. 
They are dependent on the Android framework and need to run on a device or emulator. 
Like instrumentation tests, they also go in the androidTest/ directory.
You want to test as much of your logic as you can in unit and instrumentation tests, as they run faster.
On Android, UI tests usually use the Espresso library to interface with and test the view. 

### AAA Pattern
"Arrange, Act, Assert" is a testing pattern to describe the natural phases of most software tests.
- **Arrange** describes whatever setup is needed
- **Act** describes the subject's behavior that's under test (and typically only describes a single line needed to invoke that behavior)
- **Assert** describes the verification that the subject's behavior had the desired effect by evaluating its return value or measuring a side-effect (with a spy or mock)

### ARCHITECTING FOR TESTING
Software architecture is a template or blueprint that we can use when building new apps. In the next lines I'm going to: 
- Show the characteristics of a testable architecture 
- Discover good practices to create a testable architecture.

To encourage TDD, it's better to think of a software architecture that encourages and facilitates the creation of tests, but it's not the only that matters. Also **reusable abstraction**, **early design decisions**, **design patterns** or **SOLID principles** are important to achieve a robust architecture.

#### Design Patterns
Tend to show relationships and collaboration between classes through diagrams, ideas and descriptions of to proceed when faced with similar circumstances. According the GoF you can classify DP into creational, structural and behavioral.

| Patterns | Descriptions |
| -------- | ------------ |
| Creational patterns | Explain a specific moment of time (the creation of an instance). |
| Structural patterns | Describe a static structure.|
| Behavioral patterns | Describe a dynamic flow.|


##### Creational
Describe solutions related to the object creation.

- **Singleton**. Specifies thant only one instance of a certain class may exist, known as *singleton*. It's possible to access the singleton globally.
    
    Given 

    ```sh
    object MySingleton {
        private var status = false
        private var myString = "Hello"

    fun validate(): Boolean{
        ...
    }

    ...

    }
    ```

    You can use MySingleton by invoking

    ```sh 
        MySingleton.validate()
    ```
    
    For instance, if you have an object that collaborates with MySingleton, like this:

    ```sh 
    class MyClass {
        fun methodA() {
            ...
            if (MySingleton.validate()) {
            ...      
            }...  
        }
    }
    ```
    You won’t be able to test methodA() properly because you’re using the actual MySingleton object, which means you can’t force validate() to return true or false.

- **Builder**. Abstracts the construction of a complex object, joining several parts. For example think of a restaurant that serves different menus depending on the day.
You might have the following abstract class:

    ```sh
    abstract class MenuBuilder {
    var menu = Menu()
    abstract fun buildMainDish()
    abstract fun buildDessert()
    }
    ```

    You would then implement the builders depending on the day:

    ```sh
    class DayOneMenuBuilder: MenuBuilder() {
    override fun buildMainDish() {
        // Add day one main dish to the menu
    }
    override fun buildDessert() {
        // Add day one desert to the menu
    }
    }

    class DayTwoMenuBuilder: MenuBuilder() {
    ...
    }
    ```

    You might also have the Chef class:

    ```sh
    class Chef {
    fun createMenu(builder: MenuBuilder): Menu {
        builder.buildMainDish()
        builder.buildDessert()
        return builder.menu
    }
    }
    ```

    Notice how Chef calls the corresponding methods to build the menu.
    This lets you create the menu as follows:

    ```sh
    val chef = Chef()
    val menuBuilder = getDayMenuBuilder()
    val menu = chef.createMenu(menuBuilder)
    ```

    In this example, getDayMenuBuilder() returns the corresponding MenuBuilder depending on the day.

- **DI**. Collaborators are provided to an object that requires them, instead of this object directly instantiating them internally. Dependency injection favors testability because you can inject fake collaborator objects to test different situations.
There are two ways to inject dependencies:
    - Constructor injection. Where you pass the object of the collaborator in the creator.
    - Properety or Method injection. Where you create the object without the collaborator and you can set it later. 


##### Structural
Ease the design to establish relationships between objects.
- **Adapter (or wrapper)**. Describes how to let two incompatible classes work together.
For example, when you have a list of contacts that you want to show in a RecyclereView, the Rv doesn't know how to show objects of the class Contact. That's why we need to use ContactsAdapter

- **Facade**. Defines a high-level interface object which hides the complexity of underlying objects.
    For example, we have a ProductsRepository class that provides objects of the class Product, like so:
    ```sh
    class ProductsRepository {
    ...
    fun getProducts(): List<Product> {
        if(isRemoteAvailable) {
            return api.getProducts()
        } else {
            val localProducts = room.getProducts()
            if(localProducts.isEmpty()) {
                return sharedPrefsManager.getLastProduct()
            } else {
                return localProducts
            }
        }
    }
    ```
    Here *getProducts()* grabs the data from a remote server, memory, filesystem, *Room*, *SharedPreferences*, whatever. It's easier eto user *ProductsRepository* , which abstracts getting the products from the corresponding source.

- **Composite**. To construct complex objects composed of individual parts, and to treat the individual parts and the composition uniformly. For example, *View*, *ViewGroup* and the rest of classes that inherit from *View*, anad contains a list of child *View* objects. When you ask a *ViewGroup* to *draw()*, it iterates through all of its children asking them to *draw()*

##### Behavioral
Explain how objects interact and how a task can be divided into sub-tasks among different objects. 

- **Observer**. Gives you a way to communicate between objects where one objects informs others about changes or actions. There's an **observable** object which can observe, and there's one or more **observer** objects that you use to subscribe to the observable.

- **Command**. Describes the encapsulation of an operation without knowing the real content of the operation or the receiver.
For example, if you have a drawing app where all user interactions are implemented as commands, and you put them in a stack, you can easily implement the undo by popping the commands and executing an undo() operation on the command.


#### Architectural Design Patterns

Used to achieve a robust, stable, testable, modular and easy to extend codebase.

##### MVC
Model-View-Controller states that each class you write should be part of one of the following layers:
- Model: Data classes that model your business belong to this layer. Contaian *data* and *business* logic. This layer also contains the classes that *fetch and create objects* of those business classes, *networking*, *caching* and *handling databases*.
- View: Displays data from Model. Doesn't contain business logic.
- Controller: Connect the objects of View anad Model. It gets notifiede of user interaction and updates the Model. It retrieves data from the model, It may also update the View when teheree's a change in the Model.

Ideally, you have separate layers to allow testin them separately.



              Controller    Model        View 
    --UserInput-->|--Update-->|
                  |--notifyModelUpdated--->|
                              |<--getData--|




The View knows about the Model, and the Controller has a reference to both the View and the Model.
Here's how that works:
1. The View (Activity/Fragment/Custom View) receives the user input.
2. It informs the Controller (a separate class) about it.
3. The Controller updates the Model and notifies the View that should update.
4. The View requests data from the Model.

Howevere there's still a problem: The Controller has a reference to the View, which means it won't be Unit Testable because you need to create View. The solution is to create ana interface that the View can implemente. The Controller will have a reference to the interface instead of the actual View.

##### MVP

Model-View-Presenter has these layers:
- Model: Data classes that model your business belong to this layer. Contaian *data* and *business* logic. This layer also contains the classes that *fetch and create objects* of those business classes, *networking*, *caching* and *handling databases*.
- View: Displays data presented by the Presenter ebut doesn't hace a reference to the model. It does have a areference to the Presenter to notify it about user actions. Activies/Fragments/CustomViews. Notifies the Presenter about user interactions.
- Presenter: Retrieves data from the Model anad updaates it accordingly. It hs aUI presentataion logic that adecides what to display anda notifies the View when a Model has achanged. Therefore, it has a reference to the View and the Model. Decides if has to fetch something from the Model, updates it, applies UI logic and finally tells the View what to display.

Usually, there's ana interface for the View and an interface for the Presenter, and these aare written in a single file or package sa a sort of Contract.

Differences MVC-MVP
- In MVP, the View does'nt have a direct reference to the Model, meaning it is loosely-coupled. Instead, the Presenter brings the trnasformed or simplified Model, reaady to be displayed to the View.
- MVP proposes the the Presenter should handle everything related to the presentation of the View. In MVC is not clear where the UI logic should be.

In your tests instead of using the Android classes, you could create custom classes that implement the View interfaces nada assert that the corresponding meethods were called.

For example, having this Contract:

```sh
    interface LoginPresenter {
    fun login(username: String, password: String)
    }

    interface LoginView {
        fun showLoginSuccess()
        fun showLoginError()
        fun showLoading()
    }
```

and the corresponding implementations:

```sh
    class LoginPresenterImpl(
        private val repository: LoginRepository,
        private val view: LoginView): LoginPresenter {

        override fun login(username: String, password: String) {
            view.showLoading()
            repository.login(username, password, object : Callback {
            override fun onSuccess() {
                view.showLoginSuccess()
            }
            override fun onError() {
                view.showLoginError()
            }
            })
        }
    }

    class LoginActivity: AppCompatActivity(), LoginView {

        private lateinit var presenter: LoginPresenter

        override fun onCreate(savedInstanceState: Bundle?) {
            ...

            loginButton.setOnClickListener {
            presenter.login(usernameEditText.text.toString(),
                            passwordEditText.text.toString())
            }
        }

        override fun showLoginSuccess() { ... }
        override fun showLoginError() { ... }
        override fun showLoading() { ... }
    }
```

Using JUnit, if you want to test **login** of **LoginPresenterImpl** you can create this test:

```sh
    @Test
    fun login_shouldShowLoading() {
        var didShowLoading = false

        val testRepository = object: LoginRepository { ... }

        val testView = object: LoginView {
            override fun showLoginSuccess() {}
            override fun showLoginError() {}
            override fun showLoading() { didShowLoading = true }
        }

        val presenter = LoginPresenterImpl(testRepository, testView)

        presenter.login("Foo", "1234")

        Assert.assertTrue(didShowLoading)
    }
```

Where you can create a *testRepository* and a *testView*, both implementing their corresponding interfaces. You then instantiate the *LoginPresenterImpl*, passing those test objects. Afterward, call *login()*, and *didShowLoading* gets set to *true* and your test will pass.

##### MVVM

Model-View-ViewModel contains the following layers:
- Model: Data classes that model your business belong to this layer. Contaian *data* and *business* logic. This layer also contains the classes that *fetch and create objects* of those business classes, *networking*, *caching* and *handling databases*.
- View: Notifies the ViewModel about user actions. Subscribes to streams of data exposed by the ViewModel. Activites/Fragments/CustomViews. **Consume data**.
- ViewModel: Retrieves data from the Model and updates it accordingly. Exposes streams of data ready to be displayed, but it doesn't know and doesn't care about who is subscribed to the streams. **Produces data**.

V - VM interaction is veery similar to V - P of MVP, but MVVM doesn't have a reference to the View, not even an interface. **It just exposes streams of data (Observables), it could be the Model or a transformed-displayable Model. 
The ViewModel doesn't know about the consumere, it just exposes streams of data. The View subscribes and unsubscribes to that data as needed.
Usually, the mechanism of exposing data, observing aand updating it, is done using **reaactive libraries**:
- RxKotlin/Android libraries: VM exposes **Observable** objects. V subscribes to them. When VM updates Observable data (actual Model), V will react and render the update. Important: Subscribe and unsubscribe in *OnResume()* and *onPause()*. Is using in CustomViews, the same in *onAttachedToWindow()* and *onDetachedToWindow()*.
- LiveData+ViewModel from Android Architecture Components: If your ViewModel classes extend a *ViewModel* clasas from the Android fraamework, this class has nothing to do with UI, so it's still unit testable. VM exposes *LiveData* objects and it updated the value of them. The activites have to start observing (subscribe) these *LiveDAataa* objects in the *onCreate()* method and doesn't need to stop observing. In case of Fragments, star observing in *onAactivityCreateD()* Not possible to do it with CustomViews.

For te login flow example, using ViewModel+LiveDaata from Android Architecture Components, a possible approach would be:

```sh
class LoginViewModel(
  private val repository: LoginRepository): ViewModel() {

    private val loginStatus: MutableLiveData<LoginStatus>()

    fun getLoginStatus(): LiveData = loginStatus

    fun login(username: String, password: String) {
        loginStatus.value = LoginStatus.Loading()
        repository.login(username, password, object : Callback {
            override fun onSuccess() {
                loginStatus.value = LoginStatus.Success()
            }
            override fun onError() {
                loginStatus.value = LoginStatus.Error()
            }
        })
    }
}

sealed class LoginStatus {
  class Error(): LoginStatus()
  class Success(): LoginStatus()
  class Loading(): LoginStatus()
}

```

and the Activity would do:

```sh
class LoginActivity: AppCompatActivity(), LoginView {

  private lateinit var viewModel: LoginViewModel

  override fun onCreate(savedInstanceState: Bundle?) {
    ...
    viewModel = ...

    viewModel.getLoginStatus().observe(this, Observer {
      when(it) {
        is LoginStatus.Loading -> ...
        is LoginStatus.Success -> ...
        is LoginStatus.Error -> ...
      }
    })

    loginButton.setOnClickListener {
      viewModel.login(usernameEditText.text.toString(),
                      passwordEditText.text.toString())
    }
  }
}
```

#### S.O.L.I.D Principles

Are a list of principles to build software following good practices, introduced by Robert C. Martin, aka Uncle Bob, in his *Design Principles and Design Patterns* paper.

##### Single Responsibility (SRP)
**Each class should have a unique objective** or should be **useful for a specific case**.

##### Open-closed
**The software entities of your app: classes, methods, etc, should be open for extension but closed for modification**. You should design them in such a way that adding new features or modifying behaviour shouldn't require you to modify too much of your existing code.
This can be accomplished by:
- Using class inheritance or interfaces and overriding methods.
- Delegating to other classes (dependencies) by using composition and allowing to easily exchange those classes.
For example:
```sh
class LoginActivity: AppCompatActivity(), LoginView {

  private lateinit var viewModel: LoginViewModel

  override fun onCreate(savedInstanceState: Bundle?) {
    ...
    viewModel = ...

    viewModel.getLoginStatus().observe(this, Observer {
      when(it) {
        is LoginStatus.Loading -> ...
        is LoginStatus.Success -> ...
        is LoginStatus.Error -> ...
      }
    })

    loginButton.setOnClickListener {
      viewModel.login(usernameEditText.text.toString(),
                      passwordEditText.text.toString())
    }
  }
}
```

This is what *Closed for modification but Open for extension* means.

##### Liskov substitution

**An app that uses an object of a base class should be enable to use objects of derived classes without knowing about that and continue working**. Therefore you should not be checking the subtype. In the subclass you can override some of the parent meethods as long as you continue to comply with its semantics and maintain the expected behaviour.
A good example  of a violation of this principle is that in Mathematics, a Square is a Rectangle.You may be tempted to implement this having the Square class inherit from a Rectangle class. Then, anywhere in your code where you expect a Rectangle, you could pass a Square. The problem is that in a Rectangle you can change the width or the height independently, but you cannot do that in a Square. If you want to change the width of a Square, you should override the setWidth method to also change the height to the same value. To comply with the essence of a Square, the same would apply if you want to change the height. Therefore, this implementation would be violating the principle because you would be changing the expected behavior defined in the base type, which in this case is a reshapable-rectangle.

By creating your tests first, you would realize that you cannot model a Square inheriting from a Rectangle, because the area tests you write will pass for a Rectangle but not for a Square or vice versa:

```sh
private const val WIDTH = 4
private const val HEIGHT = 3

private fun assertArea(rectangle: Rectangle) {
  Assert.assertTrue(WIDTH * HEIGHT, rectangle.area())
}

@Test
fun testAreaRectangle() {
  val rectangle = Rectangle()
  rectangle.width = WIDTH
  rectangle.height = HEIGHT

  assertArea(rectangle) // This test will pass
}

@Test
fun testAreaSquare() {
  val square = Square()
  square.width = WIDTH
  square.height = HEIGHT // This will also set square.width to HEIGHT

  assertArea(square) // Therefore, this test will fail, because area is 9
}

```

And this would be a non-violationg or good example:

```sh
interface Repository {
  fun findContactOrNull(id: String): Contact?
}

class InMemoryRepository: Repository {
  private lateinit var cachedContacts: Map<String, Contact>
  ...
  fun findContactOrNull(id: String): Contact? {
    return cachedContacts[id]
  }
}

class SqlRepository: Repository {
  fun findContactOrNull(id: String): Contact? {
    val contact = // Implementation to get it from a SQL DB
    return contact
  }
}

```

As you can see, the base interface declares a method that indicates that it would return a Contact object by id or null if it doesn’t find it. Later, the implementations, an in-memory DB and a Sql DB do what they have to do to return the Contact. Neither of them change the semantic of the interface.

##### Interface Segregation

**Encourages to create fine grained interfaces that are cleint specific**. Suppose you have a class with a few methods, one part of your app may only need acaceess a subset of your methods and other part may need to access another subset. This principle encourages you to create 2 interfaces. **Clients should haave access to only what they need and nothing more**.
TDD enforces writing more client-focused interfaces, because it makes you think from the client perspective, you avoid exposing those methods that awon't be used by the client.


##### Dependency Injection

Exposes that a **concrete class A should not depend on a concrete class B, but an abstraction of B instead**. It could be an interface or an abstract class.
When writting tests using TDD, instead of passing real collaborators (dependencies) to a class under test, it's easier to pass fake objects that conform to the same interface



